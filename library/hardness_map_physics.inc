import "mod_map";
import "mod_draw";
import "mod_mem";
include "../library/math.inc";

global
	// configuration of the global physics that affects all procceses
	hmap_gravity = 1;
	hmap_max_vertical_speed = 12;
	hmap_pixel_precision = 1; // cada cuanto pixel chequea la dureza. (si se aumenta es mas rapido pero pierde precision)
	hmap_array_mode = false;
end

local
	// physics vars of each proccees
	
	// hardness map collision box
	hmap_box_height = 0;
	hmap_box_width = 0;
	
	// process speed
	hmap_inc_x = 0; //velocidad horizontal
	hmap_inc_y = 0; //velocidad vertical
	
	// saves the las color checked on each boundary
	int hmap_collision_bottom = 0; //colision abajo
	int hmap_collision_top = 0; //colision arriba
	int hmap_collision_left = 0; //colision izquierda
	int hmap_collision_right = 0; //colision derecha		
	
	// physics properties
	float hmap_bounce_rate = 0; // how much bounces percentage (from zero to one, ex: 0.2 = 20%)
	
	// misc
	hmap_prev_y, hmap_prev_x, hmap_prev_inc_x, hmap_prev_inc_y = null;
end

type _hmap
	width = 0;
	height = 0;
	int pointer map = null; // map array	
	file;
	graph;
	float ratio = 1;
end

/**
 * init an hmap with a graph information
 */
function hmap_init(_hmap pointer hmap, int file, int graph, float ratio)
private
	i = 0, j = 0;	
begin

	hmap.height = map_info(file,graph,G_HEIGHT)*ratio; 
	hmap.width = map_info(file,graph,G_WIDTH)*ratio;
	
	hmap.file = file;
	hmap.graph = graph;
	hmap.ratio = ratio;

	

	if (hmap_array_mode)
		hmap.map = alloc(hmap.height * hmap.width * sizeof(int));
		memseti ( hmap.map , 0 , hmap.height * hmap.width);
		x = 0;
		for (i = 0; i < hmap.width; i ++)
			x += (1/ratio);
			y = 0;
			for (j = 0; j < hmap.height; j++)
				y += (1/ratio);
				hmap.map[((j * hmap.width) + i )] =  map_get_pixel(file,graph,x,y);
			end		
		end
	else
		hmap.file = 0;
		hmap.graph = map_new(hmap.width, hmap.height, map_info(file,graph,G_DEPTH), B_CLEAR );
		map_xputnp ( 0 , hmap.graph , file , graph , (hmap.width/2) , hmap.height/2 , 0 , ratio * 100 , ratio * 100 , 0 );
	end
end

function hmap_free(_hmap pointer hmap)
begin
	if (hmap_array_mode)
		mem_free(hmap.map);
		hmap.map = null;
	else
		map_unload (0,hmap.graph);
	end
end

function hmap_get_pixel(_hmap pointer hmap,int x, int y)
begin
	x = x * hmap.ratio;
	y = y * hmap.ratio;
	if ( x < hmap.width-1 and  y < hmap.height-1 and x >= 0 and y >= 0 )
		if (hmap_array_mode)
			return hmap.map[((y * hmap.width) + x )];
		else
			return map_get_pixel(hmap.file,hmap.graph,x,y);
		end
	else
		return 0;
	end
end

function hmap_collision_bottom(process_id, float p_x, float p_y,_hmap pointer hmap)
private
	i = 0, j = 0;	
begin
	
	for (i = p_x - (process_id.hmap_box_width / 8); i <= p_x + (process_id.hmap_box_width / 8); i+=hmap_pixel_precision)
		for ( j = p_y + (process_id.hmap_box_height/2); j >= (p_y); j-=hmap_pixel_precision)
			
				hmap_collision_bottom = hmap_get_pixel(hmap,i,j);
				if (hmap_collision_bottom>0)
					return hmap_collision_bottom;
				end
			
		end		
	end
	return 0;
end

function hmap_collision_top(process_id, float p_x, float p_y,_hmap pointer hmap)
private
	i = 0, j = 0;
	
begin
	
	
	for (i = p_x - (process_id.hmap_box_width / 8) ; i <= p_x + (process_id.hmap_box_width / 8) ; i+=hmap_pixel_precision)
		for (j = p_y - (process_id.hmap_box_height/2); j <= (p_y); j+=hmap_pixel_precision)
			
				hmap_collision_top = hmap_get_pixel(hmap,i,j);
				if (hmap_collision_top>0)
					return hmap_collision_top;
				end		
			
		end
	end
	return 0;
end

function hmap_collision_left(process_id, float p_x, float p_y,_hmap pointer hmap)
private
	i = 0, j = 0;
	
begin
	
	
	for (i = p_y - (process_id.hmap_box_height / 8) ; i <= p_y + (process_id.hmap_box_height / 8) ; i+=hmap_pixel_precision)
		for (j = p_x - (process_id.hmap_box_width/2); j <= (p_x); j+=hmap_pixel_precision)
			
				hmap_collision_left = hmap_get_pixel(hmap,j,i);
				if (hmap_collision_left>0)
					return hmap_collision_left;
				end
			
		end
	end
	return 0;
end

function hmap_collision_right(process_id, float p_x, float p_y,_hmap pointer hmap)
private
	i = 0, j = 0;
	
begin
	
	
	for (i = p_y - (process_id.hmap_box_height / 8);  i <= p_y + (process_id.hmap_box_height / 8); i+=hmap_pixel_precision)
		for (j = p_x + (process_id.hmap_box_width/2); j >= (p_x); j-=hmap_pixel_precision)
			
				hmap_collision_right = hmap_get_pixel(hmap,j,i);
				if (hmap_collision_right>0)
					return hmap_collision_right;
				end		
			
		end
	end
	return 0;
end


// applies physics to the process who called it. Last only 1 frame
process hmap_apply(_hmap pointer hmap)
private
	i = 0, j = 0;
	advance_step, advance_distance;
	
	float inc_x,inc_y,p_x,p_y;
	
	float value_inc_x, value_inc_y;
	
	int ip_x,ip_y;
	
begin
	// apply Gravity	
	if ((father.hmap_inc_y + hmap_gravity) < hmap_max_vertical_speed)
		father.hmap_inc_y += hmap_gravity;
	else
		father.hmap_inc_y = hmap_max_vertical_speed;
	end
	
	// actualizamos posicion
	p_x = father.x;
	p_y = father.y;
	
	value_inc_x = (float) abs(father.hmap_inc_x);
	value_inc_y = (float) abs(father.hmap_inc_y);
	
	// calculamos avance pixel por pixel
	if (value_inc_x >= value_inc_y and value_inc_x>0)
		inc_x = 1; 
	else 
		inc_x = (value_inc_x/value_inc_y);
	end
	if (value_inc_y >= value_inc_x and value_inc_y>0)
		inc_y = 1 ; 
	else
		inc_y = (value_inc_y/value_inc_x);
	end
	
	// le damos el signo
	if (father.hmap_inc_x<0) inc_x *= -1; end
	if (father.hmap_inc_y<0) inc_y *= -1; end
	
	// avanzamos pixel por pixel calculando que no nos choquemos con nadina
	advance_step = 0;
	if (value_inc_x >= value_inc_y )
		advance_distance = value_inc_x;
	else
		advance_distance = value_inc_y;
	end
	
	while( advance_step < advance_distance)
		
		// avanzamos de a un pixel aca
		p_x += inc_x;
		p_y += inc_y;

		ip_x = round(p_x);
		ip_y = round(p_y);		
		
		hmap_collision_bottom = hmap_collision_bottom(father, ip_x, ip_y +1 , hmap);
		hmap_collision_top = hmap_collision_top(father, ip_x, ip_y-1, hmap);
		hmap_collision_left = hmap_collision_left(father, ip_x-1, ip_y, hmap);
		hmap_collision_right = hmap_collision_right(father, ip_x+1, ip_y, hmap);
		
		
		// autoajuste de dureza		
		if (hmap_collision_bottom > 0)
			if (father.hmap_box_height == 0 or father.hmap_box_width == 0)
				p_y -= 0.5;
			else	
				while(hmap_collision_bottom(father, ip_x, ip_y, hmap)>0);				
					p_y = (p_y - 1);
					ip_y = (ip_y - 1);
				end
			end
		end		
		if (hmap_collision_top > 0)
			if (father.hmap_box_height == 0 or father.hmap_box_width == 0)
				p_y += 0;
			else	
				while(hmap_collision_top(father, ip_x, ip_y, hmap)>0);
					p_y = (p_y + 1);
					ip_y = (ip_y + 1);
				end
			end
		end		
		if (hmap_collision_left > 0)
			if (father.hmap_box_height == 0 or father.hmap_box_width == 0)
				p_x += 0.6;
			else	
				while(hmap_collision_left(father, ip_x, ip_y, hmap)>0);
					p_x = (p_x + 1);
					ip_x = (ip_x + 1);
				end
			end
		end		
		if (hmap_collision_right > 0)
			if (father.hmap_box_height == 0 or father.hmap_box_width == 0)
				p_x -= 0.6;
			else	
				while(hmap_collision_right(father, ip_x, ip_y, hmap)>0);
					p_x = (p_x - 1);
					ip_x = (ip_x - 1);
				end
			end
		end		
		
		advance_step ++;
	end
	
	
	
	// actualizamos banderas de colision
	father.hmap_collision_bottom = hmap_collision_bottom;
	father.hmap_collision_top = hmap_collision_top;
	father.hmap_collision_left = hmap_collision_left;
	father.hmap_collision_right = hmap_collision_right;
	
	// actualizamos posicion del padre
	father.x = round(p_x);
	father.y = round(p_y);
	
	// guarda valores para el proximo frame
	father.hmap_prev_y = father.x; 
	father.hmap_prev_x = father.y;
	father.hmap_prev_inc_y = father.hmap_inc_y; 
	father.hmap_prev_inc_x = father.hmap_inc_x;

	// rebote
	if (father.hmap_collision_bottom>0 or father.hmap_collision_top>0)	
		father.hmap_inc_y = round(father.hmap_inc_y * father.hmap_bounce_rate) * -1; // bouncing
	end
	
	if (father.hmap_collision_left>0 or father.hmap_collision_right>0)	
		father.hmap_inc_x = round(father.hmap_inc_x * father.hmap_bounce_rate) * -1; // bouncing
	end

	
end