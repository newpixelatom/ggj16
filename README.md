# Almighty Totem Spirits #
## Global Game Jam 2016 - Cordoba, Argentina ##

### About ###

It's a multiplayer game for up to 4 players, where Druids have to convert totems to their side, using a super button smashing ritual.

### Instructions ###

Place your player near a totem and press the Ritual button repeatedly until the totem is fully converted. Once the totem is converted, it will attack other druids. Convert more than 6 totems to win.

### Controls ###

* **Keyboard player 1:** WASD to move the character K to perform the ritual
* **Keyboard player 2:** Directional arrows to move the character Dot to perform the ritual
* **Gamepad:** Left Stick or Directional buttons to move the character Button 1 to perform Ritual.