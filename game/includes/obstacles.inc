// Obstacles!
process obstacle(graph,x,y)
private 
begin 
    ctype = c_scroll;
    z = 254;
    alpha = 0;   
    flags = father.flags; 
    // game loop
    loop      
        frame;
    end    
    
end

process stage_object(graph, graph_overposition)
private
    bottom;
    i;
    collision_object;
end
begin
    
    file = stage_fpg;
    ctype = c_scroll;

    i=0;

    flags = rand(0,1);

    repeat  
        //recibe que grafico hay que amuchar y se amuchan
        if (graph == graph_overposition and exists(collision_object) and collision_object.graph == graph_overposition)
            //say('soy '+ graph + ' colisiono con '+collision_object.graph);
            break;
        end
        x = rand(100,stage_width-100);
        y = rand(100,stage_height-100);
        i++;
        
        frame(0);
        //rand_seed(time());
        collision_object = collision_box(type stage_object);
    until(!collision_object or i>500);

    z = 254-y;

    if (i>500)
        signal(id, s_kill_tree);
    end

    bottom =  dummy_proccess(file,graph + 1,x,y);
    bottom.ctype = c_scroll;
    bottom.z = 256;
    bottom.flags = flags;

    obstacle(graph+2,x,y);

    switch(graph)
        case 6:
            totem_handler();
        end
    end

    loop
        if (anim_get(id))
            animate();
        end
        frame;
    end
    onexit:
    signal(id, s_kill_tree); 
end

process dummy_proccess(file,graph,x,y)
begin
    
    loop
        if (anim_get(id))
            animate();
        end
        frame;
    end
end

global
byte anim_totem_activate[11]=12,12,13,13,14,14,15,15,16,16,17,17;
byte anim_totem_active[7]=17,17,18,18,19,19,18,18;
byte anim_totem_charge[23]=20,20,21,21,22,22,23,23,24,24,25,25,26,26,27,27,28,28,29,29,30,30,31,31;
end

process totem_handler()

private
    player enemigo;
    beam;
    beam_time;
    i;
    playing_wav_tot_finish;
    charge_effect;
end
begin
    x = father.x;
    y = father.y;
    z = father.z;
    file = father.file;
    graph = father.graph;
    alpha = 0;
    ctype = c_scroll;
    beam_time = timer[0];
    flags = father.flags;

    charge_effect = dummy_proccess(file,graph,x,y);
    set_anim(charge_effect,&anim_totem_charge,sizeof(anim_totem_charge),true);
    charge_effect.flags = flags + 16;
    charge_effect.ctype = c_scroll;
    charge_effect.alpha=0;
    charge_effect.z=z-1;

    loop
         charging=false;
        

        // disparo de rayito
        if (! exists(beam) and owner!=0)
            while(enemigo = get_id(type player))
                if (enemigo.player_id == owner) continue; end
                // si el player esta cerca lo beameamos
                if (get_dist ( enemigo ) <= 100 and timer[0] - beam_time > 200 )
                    beam = totem_beam(enemigo);
                    beam_time = timer[0];
                end
            end
        end

        if (owner != 0)
            set_anim(father,&anim_totem_active,sizeof(anim_totem_active),true);
        else
            father.anim_pointer=null;
            father.graph = 6;
        end




        //para setear quien es el due~no
        for (i=0;i<=3;i++)
           

            if (conversions[i] >= 100 )
                set_anim(father,&anim_totem_activate,sizeof(anim_totem_activate),false);

                if(!playing_wav_tot_finish)
                    play_wav(totem_finish_capture[rand(0,2)],0,0);
                    playing_wav_tot_finish = true;
                end 

                // si ya tenia un owner y se lo estamos robando, le restamos un punto
                if (owner != 0)
                    totem_count[owner-1]--;
                    if (totem_count[owner-1]<0)
                        totem_count[owner-1] = 0;
                    end
                end

                // seteamos player_id del owner
                owner = i+1;

                // sumamos un punto al jugador
                totem_count[i]++;

                // reseteamos medidor de uolololo
                for (i=0;i<=3;i++)
                   conversions[i] = 0;
                end
                break;
            end
        end
        frame;

       

        if (charging)
            if (charge_effect.alpha<255)
            charge_effect.alpha+=100;
            end
        elseif(charge_effect.alpha>0)

            charge_effect.alpha-=10;
        end
    end
end

global
    byte beam_anim[9] = 1,2,3,4,5,4,5,6,7,8;
    byte beam_effect_anim[6] = 9,10,11,12,13,14,15;
end

process totem_beam(process_id)
private
    punto_x,punto_y;
    player enemigo;
    playing_wav=false;
    playing_wav_ouch=false;
end
begin
    file = totem_beam_fpg;
    set_anim(id,&beam_anim,sizeof(beam_anim),false);
    ctype = c_scroll;
    x = father.x;
    y = father.y;
    z = father.z -2;

    flags = 16;
    angle = (get_angle ( process_id)) -180000 ;
    
    graph=1;
    get_real_point( 1 , &punto_x, &punto_y );
    beam_effect(punto_x,punto_y);

    repeat
        animate();
        if(!playing_wav)
            play_wav(totem_bolt[rand(0,3)],0,2);
            playing_wav = true;
        end    
        if (enemigo=collision(type player))

            if(!playing_wav_ouch)
                play_wav(ouch[rand(0,2)],0,enemigo.player_id);
                playing_wav_ouch = true;
            end 
            enemigo.inc_x += -cos(angle)*6;
            enemigo.inc_y += sin(angle)*6;

            
            //set anim running  
            blink(enemigo,10,100);
            
            caption(enemigo, red_numbers_fnt, "-2");
            enemigo.energy -= 2;
        end

        frame;
    until(anim_end(id));

end

process beam_effect(x,y)
begin
    file = totem_beam_fpg;
    set_anim(id,&beam_effect_anim,sizeof(beam_effect_anim),false);
    ctype = c_scroll;
    flags = 16;
    z = father.z -2;
    repeat
        animate();
        frame;
    until(anim_end(id));
end