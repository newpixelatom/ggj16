
function load_game_resources()
private

begin

    system_white_small_fnt = fnt_load('res/fnt/system_white_small.fnt');
    system_colored_small_fnt = fnt_load('res/fnt/system_colored_small.fnt');  
    points_counter_fnt = fnt_load('res/fnt/menu_titles.fnt');  
    green_numbers_fnt = fnt_load('res/fnt/green_numbers.fnt');  
    red_numbers_fnt = fnt_load('res/fnt/red_numbers.fnt');  
    winner_fnt = fnt_load('res/fnt/winner.fnt');  

    stage_fpg = fpg_load('res/fpg/stage.fpg');
    player_fpg[0] = fpg_load('res/fpg/rojo.fpg');
    player_fpg[1] = fpg_load('res/fpg/amarillo.fpg');
    player_fpg[2] = fpg_load('res/fpg/verde.fpg');
    player_fpg[3] = fpg_load('res/fpg/azul.fpg');
    totem_beam_fpg = fpg_load('res/fpg/totem-beam.fpg');
    totem_energy_bar_fpg = fpg_load('res/fpg/totem_energy_bar.fpg');


    //sonidos

    chants[0] = load_wav("res/snd/GGJ2016_Chant_Loop_1.wav");   
    chants[1] = load_wav("res/snd/GGJ2016_Chant_Loop_2.wav");   
    chants[2] = load_wav("res/snd/GGJ2016_Chant_Loop_3.wav");   
    chants[3] = load_wav("res/snd/GGJ2016_Chant_Loop_4.wav");   
    chants[4] = load_wav("res/snd/GGJ2016_Chant_Loop_5.wav");

    totem_bolt[0] = load_wav("res/snd/totem_bolt_1.wav");
    totem_bolt[1] = load_wav("res/snd/totem_bolt_2.wav");   
    totem_bolt[2] = load_wav("res/snd/totem_bolt_3.wav");   
    totem_bolt[3] = load_wav("res/snd/totem_bolt_4.wav");   

    ouch[0] = load_wav("res/snd/ouch_1.wav");
    ouch[1] = load_wav("res/snd/ouch_2.wav");   
    ouch[2] = load_wav("res/snd/ouch_3.wav");  

    totem_finish_capture[0] = load_wav("res/snd/totem_capture_finish_1.wav");
    totem_finish_capture[1] = load_wav("res/snd/totem_capture_finish_2.wav");   
    totem_finish_capture[2] = load_wav("res/snd/totem_capture_finish_3.wav"); 

    respawn = load_wav("res/snd/re_spawn_1.wav");
	ingame_background = load_wav("res/snd/forest_background_loop.wav");

    menu_music = load_song("res/snd/Music_Menu_master.ogg");
end