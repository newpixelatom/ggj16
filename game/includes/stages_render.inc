process stage_renderer()
private
	
	int background_map;
	
	// rectangulo de zoom
	min_x,max_x,min_y,max_y;
	
	// rectangulo en el frame anterior (usado en easing)
	prev_min_x,prev_max_x,prev_min_y,prev_max_y;
	
	player player_id;
	size_1, size_2;
	alto_pantalla,ancho_pantalla;
	new_width,new_height;
	
	min_zoom_offset = 120; // espacio vacio minimo horizontal
	min_zoom_offset_v = 80; // minimo vertical
	
    new_size,new_center_x,new_center_y,
    
    center_x,center_y;	
	
	easing = 10;
	easing_step;
	
	stage_render_background;

	region_x, region_y;

	string stage_size;

	hardness_map_graph;
	
begin
    
	stage_render_map = map_clone(stage_fpg,1); // crea el "skin" de la pantalla

	// setea vars globales con tamanio de la pantalla 
	stage_width = graphic_info ( 0,stage_render_map , G_WIDTH );
	stage_height = graphic_info ( 0,stage_render_map , G_height );
	
	stage_render_source = map_clone ( 0,stage_render_map); // mapa usado para el scroll

	// tomamos tamaño de la resolucion
	ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );

	// calcula tamanio de la region para que el fondo no se pase de la pantaia y para matar procesos fuera
	region_x = graphic_info ( 0,stage_render_source , G_width );
	region_y = graphic_info ( 0,stage_render_source , G_height );

	if (ancho_pantalla>region_x)
		region_x=ancho_pantalla;
	end

	if (alto_pantalla>region_y)
		region_y = alto_pantalla;
	end

	define_region(1,0,0,region_x,region_y);
	
	start_scroll(0,0,stage_render_source,0,1,0,0,stage_render_map); // renderiza el scroll en un mapa (para habilitar zoom)
	
	
	file = 0;
	graph = stage_render_map;
	
	
	
	y = alto_pantalla/2;
	x = ancho_pantalla/2;
	z = 250;
	
	if (scale_mode != SCALE_NONE)
		ancho_pantalla = ancho_pantalla / 2;
		alto_pantalla = alto_pantalla / 2;
	end
	
	loop
		// detectamos zona de interes		
		min_x = max_x = min_y = max_y = null;

//		if (game_over == false)
			// zoom a todos los personajes
			while (player_id=get_id(type player))
				if (min_x == null or min_x > player_id.x)
					min_x = player_id.x;
				end			
				if (max_x == null or max_x < player_id.x)
					max_x = player_id.x;
				end
				if (min_y == null or min_y > player_id.y)
					min_y = player_id.y;
				end			
				if (max_y == null or max_y < player_id.y)
					max_y = player_id.y;
				end
			end
//		else
//			// zoom al ganador
//			player_id = players[winner - 1];
//			if (exists(player_id))
//				min_x = player_id.x;
//				max_x = player_id.x;
//				min_y = player_id.y;
//				max_y = player_id.y;
//			end
//		end
		
		// agrandamos un poco el area de interes para darle un poco mas "de aire"
		min_y -= min_zoom_offset_v;
		max_y += min_zoom_offset_v;
		min_x -= min_zoom_offset;
		max_x += min_zoom_offset;
		
		// aplicamos easing
		min_y = prev_min_y + round((min_y - prev_min_y)/ easing);
		max_y = prev_max_y + round((max_y - prev_max_y)/ easing);
		min_x = prev_min_x + round((min_x - prev_min_x)/ easing);
		max_x = prev_max_x + round((max_x - prev_max_x)/ easing);
		
		// convertimos el rectangulo a proporcion 16:9
		new_height = max_y - min_y;
		new_width = round((ancho_pantalla * (max_y - min_y)) / alto_pantalla);
		
		if (new_width < (max_x - min_x))
			new_width = max_x - min_x;
			new_height = round((alto_pantalla * (max_x - min_x)) / ancho_pantalla);
		end
		
		// vemos de no pasarnos del tamaño de la pantalla
		if (new_width > stage_width)
			new_width = stage_width;
		end
		if (new_height > stage_height)
			new_height = stage_height;
		end
		
		// actualizamos coordenadas con nuevo rectangulo
		min_x = (min_x + round((max_x - min_x) / 2)) - round(new_width/2);
		max_x = min_x + new_width;		
		min_y = (min_y + round((max_y - min_y) / 2)) - round(new_height/2);
		max_y = min_y + new_height;
		
		// mover rectangulo dentro de la pantalla
		if (min_x<0)
			max_x += (min_x * -1);
			min_x = 0;
		end		
		if (max_x>stage_width)
			min_x -= (max_x - stage_width);
			max_x = stage_width;
		end
		if (min_y<0)
			max_y += (min_y * -1);
			min_y = 0;
		end		
		if (max_y>stage_height)
			min_y -= (max_y - stage_height);
			max_y = stage_height;
		end
				
		// calculamos a donde zoomear
		new_size = round((ancho_pantalla * 100) / (max_x-min_x));
		
		new_center_x = min_x + round((max_x-min_x)/2);
		new_center_y = min_y + round((max_y-min_y)/2);
		
		set_center(file,stage_render_map,new_center_x,new_center_y);		
		size = new_size + 2; // le agregamos un piringulin como margen de error
		stage_zoom = size;

		
		frame;
		
		// guardamos valores para usarlos en el easing
		prev_min_x = min_x;
		prev_max_x = max_x;
		prev_min_y = min_y;
		prev_max_y = max_y;
	end
	
	onexit:
	
	stop_scroll(0);
	unload_map(0,stage_render_map);
	
	unload_map(0,stage_render_source);
	signal(id, s_kill_tree); // mapa el proceso que muestra el scroll
end