process menu()
public
	show_menu process_menu;
	int anim_running;
	type_effect anim;
	string menu_bg='dark';
	title;
begin
    set_game_resolution(false);
    screen_put ( stage_fpg , 34 );
        play_song(menu_music,-1);

	fade_in_slow();
	// esperamos que termine la animacion
	
	x=graphic_info ( 0,BACKGROUND , G_width )/2;
	y=graphic_info ( 0,BACKGROUND , G_height )/2;

	//agrego los botones al menu
	add_menu_btn('PLAY',system_white_small_fnt,system_colored_small_fnt,0);
    add_menu_btn('PLAYERS: '+players,system_white_small_fnt,system_colored_small_fnt,0);
	add_menu_btn('CREDITS',system_white_small_fnt,system_colored_small_fnt,0);
	add_menu_btn('EXIT',system_white_small_fnt,system_colored_small_fnt,0);

	//muestro el menu
	process_menu = show_menu(menu_bg,false);
	//z = process_menu.z+1;
	//animo los botones
	anim_buttons('in');
	
	loop
		if(menus_items[0].btn.clicked)
			game_state = 'game';
			kill_this = id;
		end
        if(menus_items[1].btn.clicked)
            players++;
        
            if (players >4)
                players =2;
            end       
            menus_items[1].text_map = write_in_map (system_white_small_fnt , 'PLAYERS: '+players ,3);
            menus_items[1].text_focus = write_in_map (system_colored_small_fnt , 'PLAYERS: '+players ,3);
            menus_items[1].btn.normal_graph = menus_items[1].text_map;
            menus_items[1].btn.focus_graph = menus_items[1].text_focus;
            menus_items[1].btn.pressed_graph = menus_items[1].text_focus;
        end 

		if(menus_items[2].btn.clicked)
			game_state = 'credits';
			kill_this = id;
		end
        if(menus_items[3].btn.clicked)
            game_state = 'exit';
            kill_this = id;
        end
		frame;
	end
	onexit:
	destroy_menu();
	signal(id, S_KILL_TREE);
end

process start_process()
private
    i;
begin
    while(exists(menu_anim_running[0]))
        //do nothing, just wait the anim to die
        //say('do nothing, anim running');
    end
    fade_out_slow();
    //delete_text(ALL_TEXT);
    if(kill_this != 0 && get_status(kill_this)>1)
        for(i = 0; i <= 10; i++)
            map_unload(0,new_text_map[i]);
            map_unload(0,new_text_focus_map[i]);
        end
        signal(kill_this, S_KILL_TREE);
        //say('killed '+kill_this );
        //signal(type item, S_KILL);
        kill_this = 0;      
    end

end

function fade_in_slow()
begin
    fade(100,100,100,5); // Fade to normal
    while(fading) frame; end // Wait for the fading to finish
end

function fade_out_slow()
begin
    fade(0,0,0,5); // Fade to black
    while(fading) frame; end // Wait for the fading to finish
end

process credits()
private
begin
    fade_in_slow();
    while(!key(_ESC))
        size_x=100;
        size_y=100;
        x = graphic_info ( 0,BACKGROUND , G_width )/2;
        y = graphic_info ( 0,BACKGROUND , G_height )/2;       
        file = stage_fpg;
        graph = 32;
        frame;        
    end 
    game_state = 'menu';
    kill_this = id;
    onexit:
        signal(id, s_kill_tree);

end
