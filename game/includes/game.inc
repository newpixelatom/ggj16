


/*
    elimina un totem de los coleccionados y resta puntaje
*/
function rest_totem(player_id)
private
totem_handler totem;
begin
    while(totem = get_id(type totem_handler))
        if (totem.owner == player_id)
            totem.owner = 0;
        end
    end
    totem_count[player_id-1]--;
    if (totem_count[player_id-1]<0)
        totem_count[player_id-1] = 0;
    end
end


// Main game process!
process game()
private 
i;
total_totems = 10;
anim;
begin
    set_game_resolution(true);

    rand_seed(time());
    stage_renderer();
    
    

    fade_in_slow();    

    for (i=1;i<=players;i++)
        totem_count[i-1] =0;
    end
    

   

    // creamos 10 totems
    from i = 1 to total_totems;
        stage_object(6,false);
    end

    // creamos 20 arbolito
    from i = 1 to 20;
        stage_object(3, 3);
    end

    // mostramos contador de totems
    for (i=1;i<=players;i++)
        write_int ( points_counter_fnt, (graphic_info ( 0,BACKGROUND , G_width ) / 6) * i , 50 , -260 , 4 , &totem_count[i-1] );
        if(i==1)
            drawing_color ( rgb(255,0,0));
        elseif(i==2)
            drawing_color ( rgb(255,255,0));
        elseif(i==3)
            drawing_color ( rgb(0,255,0));
        elseif(i==4)
            drawing_color ( rgb(0,0,255));
        end
        draw_circle[i-1] = draw_fcircle((graphic_info ( 0,BACKGROUND , G_width ) / 6) * i - 35,45,15);
    end
	
	play_wav(ingame_background,-1);
	ingame_music = load_song("res/snd/Music_Version2_Master.ogg");
	play_song(ingame_music,-1);
    set_song_volume(80);
    // game loop
    loop
        // spawnea personajes si se mueren o no existian
        for (i=1;i<=players;i++)
            if (!exists(player_instances[i-1]))
                player_instances[i-1] = player(i);
            end
        end

        frame;
        //vuelve al menu cuando presion esc
        if (key(_esc))
            game_state = 'menu';
            kill_this = id;
        end

        // revisamos por si alguien gana
        for (i=1;i<=players;i++)
            if (totem_count[i-1] > total_totems/2 )

                signal(ALL_PROCESS,s_freeze); // pausa todo el juego

                x =  graphic_info ( 0,BACKGROUND , G_width )/2;
                y =  graphic_info ( 0,BACKGROUND , G_height )/2;

                anim = caption(id, winner_fnt, player_names[i-1] + "Player Wins!");

                while (exists(anim))
                    frame;
                end

                wait(200);

                game_state = 'menu';
                kill_this = id;
                signal(ALL_PROCESS,s_wakeup); // pausa todo el juego
                return;

            end
        end
        
    end    
    onexit:
    for (i=0;i<4;i++)
        if(draw_circle[i] >1)
            delete_draw(draw_circle[i]);
        end
    end
    signal(id, s_kill_tree); // mapa el proceso que muestra el scroll
    delete_text(all_text);
	menu_music = load_song("res/snd/Music_Menu_master.ogg");
	play_song(menu_music,1);
	set_song_volume(128);
end
