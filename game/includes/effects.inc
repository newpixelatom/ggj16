Function int wait(int t)
Begin
    t += timer[0];
    While(timer[0]<t) frame; End
    return t-timer[0];
End

local
	_blink_instance = null;
end

process blink(process_id,speed,duration)
private
	i;
	last_switch;
begin
	i = timer[0];
	last_switch = timer[0];

	if (exists(process_id._blink_instance))
		return;
		//signal(process_id._blink_instance, s_kill);
	end

	process_id._blink_instance = id;
	while (((timer[0]-i)<duration or duration == 0) and exists(process_id))
		if (timer[0]-last_switch >= speed )
			last_switch = timer[0];

			if (process_id.alpha >0)
				process_id.alpha = 0;
			else
				process_id.alpha = 255;
			end
		end
		frame;
	end
	if ( exists(process_id))
	process_id.alpha = 255;
	end
end



process caption(process_id, font, string caption)
private
type_effect y_anim;
anim;
begin
	if (!exists(process_id)) return; end
	
	ctype = process_id.ctype;
	x = process_id.x;
	y = process_id.y - 10;
	z = -10;
	
	graph = write_in_map(font, caption , 4);
	
	y_anim.property = &(y);
	y_anim.effectType = motion_effect.regularEaseOut;
	y_anim.fromValue = y-20;
	y_anim.toValue = y-80;
	y_anim.duration = 35;
	anim = applyEffect(y_anim);
	
	while(exists(anim))
		
		frame;
	end

	anim = blink(id,10,100);
	while(exists(anim))
		frame;
	end	
	onexit:	
	unload_map(0,graph);
end