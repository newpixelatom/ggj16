// Player!

global
    byte anim_standing[15] = 9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12;
    byte anim_rito[1] = 3,4;
    byte anim_running[13] = 5,5,5,5,6,6,6,7,7,7,7,8,8,8;
    byte anim_dying[59] = 13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,16,16,16,16,
                            16,16,16,16,16,16,16,16,16,16,
                            16,16,16,16,16,16,16,16,16,16,
                            16,16,16,16,16,16,16,16,16,16,
                            16,16,16,16,16,16,16,16,16,16;
    byte anim_spawn[19] = 17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,25,25,26,26;
end


process player(int player_number)
private 
    press_delay=0;
    pressed=0;
    pressed_jump=false;
    totem_handler totem=0;
    totem_handler nearby_totem =0;
    totem_handler last_nearby_totem;
    totem_energy_bar energy_bar;
    playing_wav = false;
    nearby_totem_distance;
begin 
    player_id = player_number;
    file = player_fpg[player_id-1];
    
    ctype = c_scroll;

    play_wav(respawn,0,player_number);

    repeat

        x = rand(100,stage_width-100);
        y = rand(100,stage_height-100);
     
        
        frame(0);
        //rand_seed(time());
        
    until(!collision(type obstacle));

    z = 255;




    set_anim(id,&anim_spawn,sizeof(anim_spawn),false);

    // player loop
    loop  
        
       if (energy <= 0)
            status = 'dying';
            set_anim(id,&anim_dying,sizeof(anim_dying),false);
       end

        if (status != 'ritualing')
            nearby_totem = 0;
            nearby_totem_distance = 0;
            while(totem = get_id(type totem_handler))        
                if (get_dist ( totem ) <= 100 and 
                    (nearby_totem == 0 or get_dist ( totem ) < nearby_totem_distance)
                    and totem.owner != player_id)
                    nearby_totem = totem;
                    nearby_totem_distance = get_dist ( totem );
                end
            end
        end

        // Motor de estados
        switch(status):
            case "spawn":
                if (anim_end(id))
                status = "standing";
                 set_anim(id,&anim_standing,sizeof(anim_standing),true);
                end
            end
            case "standing":
                if(control(player_number,CONTROL_JUMP) and pressed_jump==false and nearby_totem!=0)
                    status = 'ritualing';
                    set_anim(id,&anim_rito,sizeof(anim_rito),true);
                    pressed = timer[0];
                    pressed_jump = true;  
                end

                if(control(player_number,CONTROL_UP) 
                    or control(player_number,CONTROL_DOWN) 
                    or control(player_number,CONTROL_LEFT) 
                    or control(player_number,CONTROL_RIGHT))
                    status = "running";
                    set_anim(id,&anim_running,sizeof(anim_running),true);
                    pressed_jump = false;
                end

                if (inc_x!=0 or inc_y !=0)
                    status = "running";
                    set_anim(id,&anim_running,sizeof(anim_running),true);
                    pressed_jump = false;
                end
                
            end
            case "running":
                
              
                //running top/down
                if(control(player_number,CONTROL_UP))
                    inc_y = -4;
                end

                if(control(player_number,CONTROL_DOWN))
                    inc_y = 4;
                end


                //running left/right
                if(control(player_number,CONTROL_LEFT))
                     inc_x = -5;
                end

                if(control(player_number,CONTROL_RIGHT))
                     inc_x = 5;
                end
        

                if(!control(player_number,CONTROL_UP) 
                    and !control(player_number,CONTROL_DOWN) 
                    and !control(player_number,CONTROL_LEFT) 
                    and !control(player_number,CONTROL_RIGHT))
                    

                    if (inc_x>0)
                        inc_x--;
                    end
                    if (inc_x<0)
                        inc_x++;
                    end 
                    if (inc_y>0)
                        inc_y--;
                    end
                    if (inc_y<0)
                        inc_y++;
                    end

                    if(inc_x == 0 and inc_y == 0)
                        status = 'standing';   
                        set_anim(id,&anim_standing,sizeof(anim_standing),true);
                    end
                end

                if(control(player_number,CONTROL_JUMP) and pressed_jump==false and nearby_totem!=0)
                    status = 'ritualing';
                    set_anim(id,&anim_rito,sizeof(anim_rito),true);
                    pressed = timer[0];
                    pressed_jump = true;  
                end

                if (inc_x>0)

                    flags = 0;
                elseif(inc_x<0)
                    flags = 1;
                end
                
            end
            
            case "ritualing":
                inc_y = 0;
                inc_x = 0;
                press_delay = timer[0]-pressed;
                if (press_delay > 30)
                    if(playing_wav)
                        stop_wav ( player_number);
                        playing_wav = false;
                    end
                    status = 'standing';
                    set_anim(id,&anim_standing,sizeof(anim_standing),true);
                    pressed_jump = false;
                else
                    if(control(player_number,CONTROL_JUMP) and pressed_jump==false and nearby_totem!=0)
                        if(nearby_totem.owner!=player_number)
                            pressed = timer[0];
                            pressed_jump = true;
                            nearby_totem.charging = true;
                            last_nearby_totem = nearby_totem;
                            last_nearby_totem.conversions[player_number-1]+=5;
                        end
                        animate();

                    elseif(!control(player_number,CONTROL_JUMP) and pressed_jump==true )
                        pressed_jump = false;
                        if (last_nearby_totem!=0)
                            last_nearby_totem.conversions[player_number-1]-=press_delay/10;
                            if (last_nearby_totem.conversions[player_number-1] < 0)
                                last_nearby_totem.conversions[player_number-1] = 0;
                            end
                        end
                        animate();
                    else
                        if (last_nearby_totem!=0)
                            last_nearby_totem.conversions[player_number-1]-=press_delay/10;
                            if (last_nearby_totem.conversions[player_number-1] < 0)
                                last_nearby_totem.conversions[player_number-1] = 0;
                            end
                        end
                    end
                    if (last_nearby_totem)
                        if(!playing_wav and nearby_totem.owner!=player_number)
                            play_wav(chants[player_number-1],0,player_number);
                            playing_wav = true;
                        end    
                        if (!exists(energy_bar) and nearby_totem.owner!=player_number)
                            energy_bar = totem_energy_bar(player_number,last_nearby_totem.conversions[player_number-1]/10);
                        end
                        //say(last_nearby_totem.conversions[player_number-1]);
                    end
                end 
            end
            
            case "dying":
                if (inc_x>0)
                        inc_x--;
                    end
                    if (inc_x<0)
                        inc_x++;
                    end 
                    if (inc_y>0)
                        inc_y--;
                    end
                    if (inc_y<0)
                        inc_y++;
                    end
                if (anim_end(id))
                    //sacar un totem
                    rest_totem(player_id);
                    //morir    
                    return;
                end
            end
        end  

        if (control(player_number,CONTROL_SHOOT))  
            say(status);
        end

        //setea los limites de la pantalla

        if (y + inc_y >= stage_height-20)
            inc_y = 0;
        end
        if (y + inc_y <= 20)
            inc_y = 0;
        end
        
        if (x + inc_x >= stage_width - 20)
            inc_x = 0;
        end
        if (x + inc_x <= 20)
            inc_x = 0;
        end 

        // mueve el personaje
        x += inc_x;
        y += inc_y;


        // si choca contra algo lo vuelve
        while (collision(type obstacle))
            if (inc_x>0)
                x--;
            end
            if (inc_x<0)
                x++;
            end 
            if (inc_y>0)
                y--;
            end
            if (inc_y<0)
                y++;
            end

            frame(0);
            if (inc_x == 0 and inc_y ==0)
            break;
            end            
        end 

        // runs animations automatically
        if (anim_get(id)!=&anim_rito)
            animate();
        end 

        frame;
    end    
    onexit:
    signal(id, s_kill_tree); 
end

process totem_energy_bar(player_number, energy)
private
showed;
end
begin
    size= 250;
    ctype = c_scroll;
    file = totem_energy_bar_fpg;
    if(energy>10)
        energy=10;
    end
    if(energy<1)
        energy=1;
    end
    player_number -=1;
    graph = energy+ (player_number*10);
    x = father.x;
    y = father.y+30; 
    showed = timer[0];
    repeat
        frame;
    until(timer[0] - showed >60);
    onexit:
    signal(id, s_kill_tree);
end