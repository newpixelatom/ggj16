
function default_controls()
private
int i, player_id = 0;
int max_joys;
begin

	max_joys = joy_number();

	player_id = 0;

	for (i=0;i<max_joys;i++)

		player_id++;
		if (player_id > 4)
			player_id = 1;
		end

		if(find(lcase(joy_name(i)),"xbox")=>0 // XBOX 360
			or find(lcase(joy_name(i)),"x-box")=>0
			or find(lcase(joy_name(i)),"xb360")=>0)

			controls_map_joy_hat(CONTROL_UP, player_id, i, 0,JOY_HAT_UP);
			controls_map_joy_hat(CONTROL_DOWN, player_id, i, 0,JOY_HAT_DOWN);
			controls_map_joy_hat(CONTROL_LEFT, player_id, i, 0,JOY_HAT_LEFT);
			controls_map_joy_hat(CONTROL_RIGHT, player_id, i, 0,JOY_HAT_RIGHT);
			
			controls_map_joy_axis(CONTROL_UP, player_id, i, 1, -10000);
			controls_map_joy_axis(CONTROL_DOWN, player_id, i, 1, 10000);
			controls_map_joy_axis(CONTROL_LEFT, player_id, i, 0, -10000);
			controls_map_joy_axis(CONTROL_RIGHT, player_id, i, 0, 10000);
			
			controls_map_joystick(CONTROL_SHOOT, player_id, i, 0); // boton A xbox gamepad
			controls_map_joystick(CONTROL_JUMP, player_id, i, 1); // boton B xbox gamepad
			
			controls_map_joystick(CONTROL_OK, player_id, i, 0); // boton O en ouya
			controls_map_joystick(CONTROL_CANCEL, player_id, i, 1); // boton A en ouya		
		elseif(find(lcase(joy_name(i)),"hid gamepad")=>0) // amazon

			controls_map_joy_hat(CONTROL_UP, player_id, i, 0,JOY_HAT_UP);
			controls_map_joy_hat(CONTROL_DOWN, player_id, i, 0,JOY_HAT_DOWN);
			controls_map_joy_hat(CONTROL_LEFT, player_id, i, 0,JOY_HAT_LEFT);
			controls_map_joy_hat(CONTROL_RIGHT, player_id, i, 0,JOY_HAT_RIGHT);
			
			controls_map_joy_axis(CONTROL_UP, player_id, i, 1, -10000);
			controls_map_joy_axis(CONTROL_DOWN, player_id, i, 1, 10000);
			controls_map_joy_axis(CONTROL_LEFT, player_id, i, 0, -10000);
			controls_map_joy_axis(CONTROL_RIGHT, player_id, i, 0, 10000);
			
			controls_map_joystick(CONTROL_SHOOT, player_id, i, 3); // boton A xbox gamepad
			controls_map_joystick(CONTROL_JUMP, player_id, i, 0); // boton B xbox gamepad
			
			controls_map_joystick(CONTROL_OK, player_id, i, 0); // boton O en ouya
			controls_map_joystick(CONTROL_CANCEL, player_id, i, 1); // boton A en ouya		
		
		elseif(find(lcase(joy_name(i)),"ouya")=>0) // OUYA

			if (os_id == 1003) // OUYA (android) en ouya los controles se mapean diferente (porque hicieron eso D: ??)
				controls_map_joystick(CONTROL_UP, player_id, i, 0);
				controls_map_joystick(CONTROL_DOWN, player_id, i, 1);
				controls_map_joystick(CONTROL_LEFT, player_id, i, 2);
				controls_map_joystick(CONTROL_RIGHT, player_id, i, 3);

				controls_map_joy_axis(CONTROL_UP, player_id, i, 1, -10000);
				controls_map_joy_axis(CONTROL_DOWN, player_id, i, 1, 10000);
				controls_map_joy_axis(CONTROL_LEFT, player_id, i, 0, -10000);
				controls_map_joy_axis(CONTROL_RIGHT, player_id, i, 0, 10000);		
				
				controls_map_joystick(CONTROL_OK, player_id, i, 5); // boton O en ouya
				controls_map_joystick(CONTROL_CANCEL, player_id, i, 6); // boton A en ouya		
				
				controls_map_joystick(CONTROL_JUMP, player_id, i, 5); // boton O en ouya
				controls_map_joystick(CONTROL_SHOOT, player_id, i, 8); // boton U en ouya
				
				controls_map_joystick(CONTROL_CONFIG, player_id, i, 9); // boton Y en ouya

				controls_map_joystick(CONTROL_MENU, player_id, i, 16); // righ stick press en ouya
			else
				controls_map_joystick(CONTROL_UP, player_id, i, 8);
				controls_map_joystick(CONTROL_DOWN, player_id, i, 9);
				controls_map_joystick(CONTROL_LEFT, player_id, i, 10);
				controls_map_joystick(CONTROL_RIGHT, player_id, i, 11);

				controls_map_joy_axis(CONTROL_UP, player_id, i, 1, -10000);
				controls_map_joy_axis(CONTROL_DOWN, player_id, i, 1, 10000);
				controls_map_joy_axis(CONTROL_LEFT, player_id, i, 0, -10000);
				controls_map_joy_axis(CONTROL_RIGHT, player_id, i, 0, 10000);		
				
				controls_map_joystick(CONTROL_OK, player_id, i, 0); // boton O en ouya
				controls_map_joystick(CONTROL_CANCEL, player_id, i, 3); // boton A en ouya		
				
				controls_map_joystick(CONTROL_SHOOT, player_id, i, 1); // boton O en ouya
				controls_map_joystick(CONTROL_JUMP, player_id, i, 0); // boton U en ouya
				
				controls_map_joystick(CONTROL_CONFIG, player_id, i, 2); // boton Y en ouya
				controls_map_joystick(CONTROL_MENU, player_id, i, 7); // righ stick press en ouya
			end


		else // Generic

			controls_map_joy_hat(CONTROL_UP, player_id, i, 0,JOY_HAT_UP);
			controls_map_joy_hat(CONTROL_DOWN, player_id, i, 0,JOY_HAT_DOWN);
			controls_map_joy_hat(CONTROL_LEFT, player_id, i, 0,JOY_HAT_LEFT);
			controls_map_joy_hat(CONTROL_RIGHT, player_id, i, 0,JOY_HAT_RIGHT);
			
			controls_map_joy_axis(CONTROL_UP, player_id, i, 1, -10000);
			controls_map_joy_axis(CONTROL_DOWN, player_id, i, 1, 10000);
			controls_map_joy_axis(CONTROL_LEFT, player_id, i, 0, -10000);
			controls_map_joy_axis(CONTROL_RIGHT, player_id, i, 0, 10000);
			
			controls_map_joystick(CONTROL_SHOOT, player_id, i, 2); // boton A xbox gamepad
			controls_map_joystick(CONTROL_JUMP, player_id, i, 0); // boton B xbox gamepad
			
			controls_map_joystick(CONTROL_OK, 	   player_id, i, 0); // boton O en ouya
			controls_map_joystick(CONTROL_CANCEL, player_id, i, 1); // boton A en ouya		
		end			
	end

	/*
	keyboard
	*/

	// global game controls
	
	controls_map_keyboard(CONTROL_MENU, 1, _ESC);
	controls_map_keyboard(CONTROL_MENU, 1, _END);


	player_id++;
	if (player_id>4)
		player_id = 1;
	end

	// player 1
	controls_map_keyboard(CONTROL_LEFT, player_id, _a);
	controls_map_keyboard(CONTROL_OK, player_id, _j);
	controls_map_keyboard(CONTROL_OK, player_id, _enter);
	controls_map_keyboard(CONTROL_RIGHT, player_id, _d);
	controls_map_keyboard(CONTROL_DOWN, player_id, _s);
	controls_map_keyboard(CONTROL_SHOOT, player_id, _j);
	controls_map_keyboard(CONTROL_JUMP, player_id, _k);
	controls_map_keyboard(CONTROL_UP, player_id, _w);
	controls_map_keyboard(CONTROL_CANCEL, player_id, _b);
	controls_map_keyboard(CONTROL_CONFIG, player_id, _y);
	controls_map_keyboard(CONTROL_MENU, player_id, _ESC);

	player_id++;
	if (player_id>4)
		player_id = 1;
	end

	// player 2
	controls_map_keyboard(CONTROL_LEFT, player_id, _left);
	controls_map_keyboard(CONTROL_RIGHT, player_id, _right);
	controls_map_keyboard(CONTROL_DOWN, player_id, _down);
	controls_map_keyboard(CONTROL_SHOOT, player_id, _0);
	controls_map_keyboard(CONTROL_JUMP, player_id, _point);
	controls_map_keyboard(CONTROL_UP, player_id, _up);
	controls_map_keyboard(CONTROL_OK, player_id, _0);
	controls_map_keyboard(CONTROL_CANCEL, player_id, _1);
	controls_map_keyboard(CONTROL_CONFIG, player_id, _3);
	controls_map_keyboard(CONTROL_MENU, player_id, _2);
		
	
end