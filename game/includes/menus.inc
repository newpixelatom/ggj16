global
	Struct menus_items[10]
		text_map;
		text_focus;
		ui_button btn;
		play_wav = true;
	end
	show_menu current_menu;
	dummy_text menu_title;
end
//return last menu item so we keep adding at the end
function menus_next_item()
private 
	var c=0;
	last_item;
begin
	//arranco con el last item -1 despues se incrementa
	last_item = -1;
	for(c=0;c<10;c++)
		if(exists(menus_items[c].btn))
			last_item = c;	
		end
	end
	return last_item+1;
end

//add menu item. Basically add the btn configuration
function add_menu_btn(string btn_text, btn_font_normal,btn_font_focus, owner)
private
var next_item;
begin
	next_item = menus_next_item();
	//say('add item '+next_item);
	if(next_item<10)
		//say('btn info '+btn_text+' btn_font' + btn_font_normal);
		menus_items[next_item].text_map = write_in_map (btn_font_normal , btn_text ,3);
		menus_items[next_item].text_focus = write_in_map (btn_font_focus , btn_text ,3);
		//create btn
		menus_items[next_item].btn = ui_button(1,
												menus_items[next_item].text_map,
												menus_items[next_item].text_focus,
												menus_items[next_item].text_focus,
												500,
												next_item*20,
												owner);
		signal(menus_items[next_item].btn, s_sleep);
		//say('sleep_item '+next_item);
	end
end


global
	int title_anim_running[1];
end
process add_menu_title(string d_text, d_font, string position, string effect)
private
	title;
	ancho_pantalla;
	alto_pantalla;
	type_effect title_anim;
	type_effect title_anim_2;

	parent;
end
begin
	//return;
	//map_clear(0,graph,rgb(0,0,0,16));
	ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
	if(position=='top_middle')
		x=ancho_pantalla/2;
		y=40;
	end
	
	graph = write_in_map(d_font,d_text,4);

	parent = get_id (type show_menu);
	if(exists(parent))
		z = parent.z -10;
	else
		z=-10;
	end	

	//esto crashea tb
	
	if(effect != '' && !exists(title_anim_running[0]) && !exists(title_anim_running[1]))
		if(!exists(title_anim_running[1]))
			title_anim_2.property = &size_x;
			title_anim_2.effectType = motion_effect.elasticEaseIn;
			title_anim_2.fromValue = 5;
			title_anim_2.toValue = 100;
			title_anim_2.duration=10;
			title_anim_running[1]  = applyEffect(&title_anim_2);

			title_anim.property = &angle;
			title_anim.effectType = motion_effect.bounceEaseInOut;
			title_anim.fromValue = 5000;
			title_anim.toValue = -300;
			title_anim.duration=10;
			title_anim_running[0]  = applyEffect(&title_anim);
		end
	end
	
	loop
		frame;
	end
	onexit:
		unload_map(0,graph);
end


//process showing the menu in the middle of the screen
process show_menu(string bg, bool hd)
public	
	int anim_running;
private 
	var c=0;
	var pos_x;
	var pos_y;
	largest_size_x=0;
	ancho_pantalla;
	alto_pantalla;
	item_size;
	offset_y;
	type_effect menu_anim_bg;
	int menu_anim_bg_running;
	int width;
	first_time_sound=true;
begin
	ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
	x=ancho_pantalla/2;
	y=alto_pantalla/2;
	offset_y=15;
	
	for(c=0;c<10;c++)
		if(exists(menus_items[c].btn))

			//saco el ancho del grafico del presente proceso
			item_size = graphic_info(0,menus_items[c].btn.normal_graph,G_WIDTH);
			//say('current_btn_size: '+item_size);
			if(hd)			
				pos_x = x - item_size;
			else	
				pos_x = x - item_size/2;
			end

			if(c==0)
				pos_y = y + menus_items[c].btn.y + offset_y;
			else
				pos_y = menus_items[c-1].btn.y + offset_y;
			end	

			if(hd)
				pos_y +=20;
				menus_items[c].btn.size=200;
			end
			menus_items[c].btn.x = pos_x;
			menus_items[c].btn.y = pos_y;

			//get largest
			if(item_size > largest_size_x)
				if(hd)			
					largest_size_x = item_size*2;
				else	
					largest_size_x = item_size;
				end
			end
		end
	end
	//set size to the menu based on the numbers of buttons and their sizes
	//say('largest '+ largest_size_x);
	width = largest_size_x +30;
	if(hd)
		size_x=200;
	else	
		size_x=100;
	end

	//size_y = alto_pantalla;
	graph = map_new(width,alto_pantalla,16);
	alpha = 70;
	if(bg =='light')
		map_clear(0,graph,rgb(250,250,250,16));
	elseif(bg =='dark')
		map_clear(0,graph,rgb(0,0,0,16));
	elseif(bg =='darker')
		map_clear(0,graph,rgb(0,0,0,16));
		alpha=150;
	elseif(bg =='light-red')
		map_clear(0,graph,rgb(250,150,150,16));	
	elseif(bg =='light-green')
		map_clear(0,graph,rgb(150,250,150,16));	
	elseif(bg =='light-blue')
		map_clear(0,graph,rgb(150,150,250,16));	
	elseif(bg =='transparent')
		alpha = 0;
	end	
	wakeup_menu_items();

		menu_anim_bg.property = &size_x;
		menu_anim_bg.effectType = motion_effect.elasticEaseOut;
		menu_anim_bg.fromValue = 1;
		//menu_anim_bg.toValue = graphic_info(0,graph,G_WIDTH);
		menu_anim_bg.toValue = size_x;
		menu_anim_bg.duration=50;
		menu_anim_bg_running  = applyEffect(&menu_anim_bg);

		while(exists(menu_anim_bg_running))
			frame;
		end
	
	loop
		for(c=0;c<10;c++)
			// PARA QUE NO SUENE EN EL PRIMER BOTON POR DEFECTO
			if(first_time_sound && c==0)
				menus_items[0].play_wav=false;
			end
			if(exists(menus_items[c].btn))
				//que reproduzca una sola vez el wav cuando esta en focus
				if(menus_items[c].btn.focus && menus_items[c].play_wav)
						//play_this_wav(snd_menu_selection,0,-1);
						menus_items[c].play_wav=false;
				elseif(!menus_items[c].btn.focus) //si se va del focus, lo bandereamos para que vuelva a reproducir
					menus_items[c].play_wav=true;
					first_time_sound=false;
				end
			end	
		end	
		frame;
	end
	onexit:
		destroy_menu();
end

function wakeup_menu_items()
private 
	var c=0;
begin
	for(c=0;c<10;c++)
		if(exists(menus_items[c].btn))
			//say('waking up '+c);
			signal(menus_items[c].btn, s_wakeup);
		end
	end
	controls_enable(0);
end

global
type_effect menu_anim[9];
int menu_anim_running[9];
end

function destroy_menu()
private 
	var c=0;
begin
	for(c=0;c<10;c++)
		while(exists(menu_anim_running[0]))
			//frame;
		end
		if(exists(menus_items[c].btn))
			unload_map(0,menus_items[c].text_map);
			menus_items[c].text_map=0;
			unload_map(0,menus_items[c].text_focus);
			if(exists(menus_items[c].btn))
				//say('killing ' +c);
				signal(menus_items[c].btn, S_KILL_TREE);
			end			
		end
	end
end

function anim_buttons(string direction)

local
	int from_y;
	int c=0;
	int to_y;
	int alto_pantalla;
end
begin
	return false;
	controls_disable(0);
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
	//say(direction);
	for(c=0;c<10;c++)

		if(exists(menus_items[c].btn))

				if(direction == 'in')
					//from_y = (c*10);					
					from_y = 0;					
					from_y -= 200;
					to_y = menus_items[c].btn.y;
					menu_anim[c].effectType = motion_effect.regularEaseOut;
					menu_anim[c].duration= 30 - (c*5);
				elseif(direction == 'out')
					from_y = menus_items[c].btn.y;
					to_y = alto_pantalla + (c*20);
					menu_anim[c].effectType = motion_effect.regularEaseIn;
					menu_anim[c].duration= 20- (c*5);
				end;

					if(from_y < -1000)
						//say('from a la bosta: '+from_y+' de item '+c );
						//say('anim '+c+' from val y '+ from_y +' to_val y '+to_y);
					end
					
					if(to_y<0)
						//say('to a la bosta: '+to_y+' de item '+c );
						//say('anim '+c+' from val y '+ from_y +' to_val y '+to_y);
					end

				//from_y=0;
				
				// if(from_y < -1000)
				//		from_y = -700;	
				//	end

				// configuramos una animacion 
				menu_anim[c].property = &menus_items[c].btn.y;
				menu_anim[c].fromValue = from_y;
				menu_anim[c].toValue = to_y;
				menu_anim_running[c]  = applyEffect(&menu_anim[c]);
				//say('anim '+c+' from val y '+ from_y +' to_val y '+to_y);
		end
	end
	while (exists(menu_anim_running[0])) 
		if(direction == 'out')
			menus_items[0].btn.alpha-=10;
		end
		frame; 
	end
	controls_enable(0);
	frame;
end
