
// el juego siempre corre en HD, pero la resolucion del juego puede ser otra
// si no se pasa HD, el juego se escala, si se pasa HD, el juego corre en HD 
// en forma nativa
global
    bool HD_ON = false;
    bool init_res = false;
end
function set_game_resolution(bool HD)
private
	dsx, dsy;	
	resolution_width, resolution_height;
end
begin
    // no need to change resolution
    
	get_desktop_size(&dsx, &dsy);

	if (!full_screen)
		dsx = 1280;
		dsy = 720;
	end

	if (dsx != 1280 or dsy != 720)
		set_mode(1280, 720, 16);
	end

	scale_resolution = 1280 * 10000 + 720 ;
	scale_resolution_aspectratio = SRA_PRESERVE;
	
	
	
    if (!HD)
        // escalado de pantalla
        resolution_width = 640;
        resolution_height = 360;
        
        HD_ON = false;
    else
        scale_resolution = -1;

		resolution_width = 1280;
        resolution_height = 720;
		
        HD_ON = true;
    end
    
    set_mode(resolution_width, resolution_height, 16);
    init_res = true;
end

function get_current_color_depth()
begin
	return graphic_info ( 0,BACKGROUND , G_DEPTH );
end

/**
 * Bugfix para cuando se pasa a segundo plano
 */
global
	focus_control_return_to_full_screen = false;
end
function focus_control()
private
ancho_pantalla;
alto_pantalla;
begin	
	if(!focus_status and full_screen )
		ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
		alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
		full_screen = false;
		set_mode(ancho_pantalla,alto_pantalla,get_current_color_depth());
		focus_control_return_to_full_screen = true;
		signal(current_status,S_FREEZE_TREE);
		//	TODO: bajar frame rato, pausar la musica, etc (esta todo en el MvR)
	elseif(focus_status and !full_screen and focus_control_return_to_full_screen)
		full_screen = true;
		ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
		alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
		set_mode(ancho_pantalla,alto_pantalla,get_current_color_depth());
		signal(current_status,S_WAKEUP_TREE);
		focus_control_return_to_full_screen = false;
	end
end
