import "mod_time"
import "mod_map";
import "mod_grproc";
import "mod_key";
import "mod_say";
import "mod_wm";
import "mod_video";
import "mod_scroll";
import "mod_mouse";
import "mod_proc";
import "mod_draw";
import "mod_rand";
import "mod_math";
import "mod_timers";
import "mod_text";
import "mod_screen";
import "mod_string";
import "mod_file";
import "mod_dir";
import "mod_sound";
import "mod_mem";
import "mod_blendop";

// opciones de compilacion

include "../library/hardness_map_physics.inc";
include "../library/motion_tween.inc";
include "../library/math.inc";
include "../library/controls.inc";
include "../library/ui/buttons.inc";
include "../library/animation.inc";

global
    string game_state = "menu";
    int current_game = 0;
    int kill_this = 0;
    current_status;
    system_white_small_fnt;
    system_colored_small_fnt;
    points_counter_fnt;
    green_numbers_fnt;
    red_numbers_fnt;
    winner_fnt;
    stage_fpg;
    player_fpg[3];
    string player_names[3]="Red","Yellow","Green","Blue";
    totem_beam_fpg;
    totem_energy_bar_fpg;
    players=2;
    draw_circle[3]=0;

    stage_render_map; // mapa con el scroll renderizado
    stage_render_source; // mapa con la pantalla limpia renderizada
    stage_render_proccess; // proceso que renderiza la pantalla y hace zoom
    stage_zoom; // zoom actual aplicado a la pantalla
    stage_width;
    stage_height;

    chants[4];
    totem_bolt[3];
    ouch[2];
    totem_finish_capture[2];
    respawn;
	ingame_music;
	menu_music;
	ingame_background;
	
    new_text_map[10], new_text_focus_map[10];
end

global
    // contador de puntaje
    int totem_count[3] = 0,0,0,0;

    player_instances[3];
end


const
    CONTROL_SHOOT = 16;
    CONTROL_JUMP = 17;
    CONTROL_CONFIG = 18;
    CONTROL_EXIT = 19;
    CONTROL_MENU = 20;    
end

include "includes/declares.inc";
include "includes/screen.inc";
include "includes/resources.inc";
include "includes/effects.inc";
include "includes/obstacles.inc";
include "includes/stages_render.inc";
include "includes/game.inc";
include "includes/player.inc";
include "includes/menus.inc";
include "includes/flow.inc";
include "includes/defaultcontrols.inc";

Process Main()
private	

Begin
    set_fps(30,0);
    full_screen = true;    
    set_game_resolution(false);

    load_game_resources();
    // configuracion inicial
    default_controls();

	
	set_song_volume(128);
				
    // mostramos imagen de caratula

    screen_put ( stage_fpg , 33 );
    fade_in_slow();

    wait(400);
    fade_out_slow();

	// corre el juego
	loop

        switch (game_state):
            case "menu":
                anim_buttons("out");
                start_process();
                current_status = menu();
            end
            case "game":
                start_process();
                current_status = game();
            end
            case "credits":
                start_process();
                current_status = credits();
            end
            case "exit":
                exit();
            end
        end

        Repeat
            focus_control(); // esto deberia estar en un loop justo con el juego principal

            if (exit_status) 
                exit();  // cierra app
            end
            
            frame;
        Until(!exists(current_status) or current_status == kill_this)

	end	
    
End